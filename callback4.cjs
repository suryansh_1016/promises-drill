const boardData = require("./data/boards_1.json");

const getboard = require("./callback1.cjs");
const getlist = require("./callback2.cjs");
const getCards = require("./callback3.cjs");




function problem4(name, stone) {

    return new Promise((resolve, reject) => {
        setTimeout(() => {

            let mindId = null;
            const board = boardData.find(element => element.name === name);
            const boardId = board.id;

            getboard(boardId)
                .then(boardResult => {
                    console.log(boardResult);
                    return getlist(boardResult.id);

                })
                .then(listResult => {
                    console.log(listResult);

                    listResult.find((element) => {
                        if (element.name === stone) {
                            mindElement = element.id;
                        }
                    });

                    if(mindElement){
                        return getCards(boardId);
                    }
                    else{
                        reject("error occured");
                        return;
                    }
                })
                .then((finalResult)=>{
                    const mindEntry = Object.entries(finalResult).find((ele) => ele[0] === mindElement);
                    
                    if(mindEntry){
                        resolve(mindEntry[1]);
                    }else{
                        reject("mind not find");
                    }
                    
                    })
                
                .catch(error => {
                    console.log(error);
                })
                

        }, 2000);


    });

}

module.exports = problem4;