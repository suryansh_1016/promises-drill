const listData = require("./data/lists_1.json");
const cardsData = require("./data/cards_1.json");


function problem3(listId) {

    return new Promise((resolve, reject) => {
        setTimeout(() => {

            let data = {};

            for (let keys in listData) {
                if (keys === listId) {
                    let newData = listData[keys];
                    for (let key in newData) {
                        if (cardsData[newData[key].id]) {
                            data[newData[key].id] = cardsData[newData[key].id];
                        }
                    }
                }
            }

            if (Object.keys(data).length != 0) {
                resolve(data);
            }
            else {
                reject("error occured");
            }

        }, 2000);
    });

}

module.exports = problem3;