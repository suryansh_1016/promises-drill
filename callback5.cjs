const boardData = require("./data/boards_1.json");

const getboard = require("./callback1.cjs");
const getlist = require("./callback2.cjs");
const getCards = require("./callback3.cjs");




function problem5(name , stone1 , stone2 ) {

    return new Promise((resolve, reject) => {
        setTimeout(() => {

            let mindElement = null;
            let stoneElement = null;
            const board = boardData.find(element => element.name === name);
            const boardId = board.id;

            getboard(boardId)
                .then(boardResult => {
                    console.log(boardResult);
                    return getlist(boardResult.id);

                })
                .then(listResult => {
                    console.log(listResult);

                    listResult.find((element) => {
                        if (element.name === stone1) {
                            mindElement = element.id;
                        }
                        if (element.name === stone2){
                            stoneElement = element.id;
                        }
                    });

                    if(mindElement || stoneElement){
                        return getCards(boardId);
                    }
                    else{
                        reject("error occured");
                        return;
                    }
                })
                .then((finalResult)=>{
                    const res={};
                    Object.entries(finalResult).find((ele) => {
                        if(ele[0] === mindElement){
                            res[mindElement]=ele[1];
                        }
                        if(ele[0] === stoneElement){
                            res[stoneElement]=ele[1];
                        }
                    });
                    resolve(res);
                    
                    })
                
                .catch(error => {
                    console.log(error);
                })
                

        }, 2000);


    });

}

module.exports = problem5;