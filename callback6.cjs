const boardData = require("./data/boards_1.json");

const getboard = require("./callback1.cjs");
const getlist = require("./callback2.cjs");
const getCards = require("./callback3.cjs");

function problem6(name) {

    return new Promise((resolve, reject) => {
        setTimeout(() => {

            const board = boardData.find(element => element.name === name);
            const boardId = board.id;

            getboard(boardId)
                .then(boardResult => {
                    console.log(boardResult);
                    return getlist(boardResult.id);
                })
                .then(listResult => {
                    console.log(listResult);
                    return getCards(boardId);
                })
                .then(cardResult=>{

                    resolve(cardResult);
                })
                .catch(error => {
                    reject("error occured");
                })
                

        }, 2000);


    });

}

module.exports = problem6;