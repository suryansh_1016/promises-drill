const problem3 = require("../callback3.cjs");

const listId = "mcu453ed";

problem3(listId)
    .then(result=>{
        console.log(result);
    })
    .catch(error=>{
        console.log(error);
    })
