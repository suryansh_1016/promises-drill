const problem1 = require("../callback1.cjs");

const boardIdToFind = "mcu453ed";

problem1(boardIdToFind)
    .then(result => {
        console.log(result);
    })
    .catch(error=>{
        console.log(error);
    })
