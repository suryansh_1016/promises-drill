const problem2 = require("../callback2.cjs");

const listIdToFind = "mcu453ed";

problem2(listIdToFind)
    .then(result=>{
        console.log(result);
    })
    .catch(error=>{
        console.log(error);
    })