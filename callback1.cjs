const board = require("./data/boards_1.json");

function problem1(boardIdToFind){

    return new Promise((resolve , reject)=>{
        
        setTimeout(()=>{
             const result = board.find(element => element.id === boardIdToFind);
             if(result){
                resolve(result);
             }
             else{
                reject( "Error Occured : 'Board not found' " );
             }
        } , 2000);
    });
}

module.exports = problem1;