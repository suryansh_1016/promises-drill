const list  = require("./data/lists_1.json");

function problem2(listId){

    return new Promise((resolve , reject)=>{

        setTimeout(() => {
let result = null;
Object.entries(list).forEach(([key,value])=>{
    if(key === listId){
        result = value;
    }
});
            
            if(result != null){
                resolve(result);
            }
            else{
                reject("error occures , no such list found");
            }
        }, 2000);
    });

}

module.exports = problem2;